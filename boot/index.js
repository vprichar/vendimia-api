import to from 'await-to';
import Bang from 'bang';
import data from './data'

export async function boot (app) {

  const Configuration = app.models.configuration;
  const Article = app.models.article;
  const Client = app.models.client;

  const { data: configuration, err: errConfigFind } = await to(Configuration.findOne());
  if (errConfigFind) throw Bang.wrap(errConfigFind);
  if (!configuration) {
    const { data: newConfiguration, err: errConfigurationCreate } = await to(Configuration.create(data.configuration));
    if (errConfigurationCreate) throw Bang.wrap(errConfigurationCreate);
    console.log('Default configuration was created successfully');
  }

  const { data: articles, err: errArticleFind } = await to(Article.findOne());
  if (errArticleFind) throw Bang.wrap(errArticleFind);
  if (!articles) {
    const { data: newArticles, err: errArticleCreate } = await to(Article.create(data.articles));
    if (errArticleCreate) throw Bang.wrap(errArticleCreate);
    console.log('Default articles was created successfully');
  }

  const { data: clients, err: errClientFind } = await to(Client.findOne());
  if (errClientFind) throw Bang.wrap(errClientFind);
  if (!clients) {
    const { data: newClients, err: errClientCreate } = await to(Client.create(data.clients));
    if (errClientCreate) throw Bang.wrap(errClientCreate);
    console.log('Default clients was created successfully');
  }

}
