export default {
  port: process.env.PORT || 5000,
  email: {
    from: 'AppName ✉️',
    service: 'gmail',
    auth: {
      user: 'user@gmail.com',
      pass: 'password',
    }
  },
  paths: {
    app: 'urlAppPath',
    api: 'https://vendimia.herokuapp.com'
  },
  roles: {
    "1": { id: 1, name: 'user' }
  },
  cors: {
    origin: '*',
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
  }
}
