import to from 'await-to';
import config from '../../config'
import Bang from 'bang';

export async function ensureUser (ctx, next) {
  const User = ctx.app.models.user;
  const AccessToken = ctx.app.models.accessToken;

  const token = ctx.header.authorization;
  if (!token) return next();

  const { data: accessToken, err } = await to(AccessToken.findOne({ where: { token: token } }));
  if (err) throw Bang.wrap(err);
  if (!accessToken) return next();

  const currentDate = new Date();
  const creationDate = new Date(accessToken.created);
  const expirationDate = creationDate.setSeconds(creationDate.getSeconds() + accessToken.ttl);
  const isTokenAlive = (currentDate.getTime() < creationDate.getTime());
  if (!isTokenAlive) return next();

  const { data: user, err: errUser } = await to(User.findOne({ where: { id: accessToken.userId } }));
  if (errUser) throw Bang.wrap(errUser);
  if (!user) return next();

  user.role = config.roles[user.roleId];

  ctx.state.user = user;

  return next();
}
