import _ from 'lodash';
import to from 'await-to';
import Bang from 'bang';
import config from '../../../config'

export async function getArticles (ctx) {
  const Article = ctx.app.models.article;
  const { query } = ctx.query;

  const { data: articles, err } = await to(Article.find());
  if (err) throw Bang.wrap(err);

  if (query) {
    var regex = new RegExp(query, 'i')

    const filteredArticles = _.filter(articles, (article) => {
      const valid = regex.test(article.description);
      return valid;
    });

    ctx.body = filteredArticles;
  }
  else {
    ctx.body = articles;
  }
}

export async function getById (ctx) {
  const Article = ctx.app.models.article;
  const articleId = ctx.params.articleId;

  const { data: article, err } = await to(Article.findById(articleId));
  if (!article) throw Bang.notFound('article with provided id does not exist');
  if (err) throw Bang.wrap(err);

  ctx.body = article;
}
