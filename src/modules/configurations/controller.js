import _ from 'lodash';
import to from 'await-to';
import Bang from 'bang';
import config from '../../../config'

export async function createConfiguration (ctx) {
  const Configuration = ctx.app.models.configuration;
  const configuration = ctx.request.body;

  const { data: newConfiguration, err } = await to(Configuration.create(configuration));
  if (err) throw Bang.wrap(err);

  ctx.body = newConfiguration;
}


export async function getConfiguration (ctx) {
  const Configuration = ctx.app.models.configuration;

  const { data: configuration, err } = await to(Configuration.findOne());
  if (err) throw Bang.wrap(err);
  if (!configuration) throw Bang.notFound('configuration does not exist');

  ctx.body = configuration;
}
