import _ from 'lodash';
import to from 'await-to';
import Bang from 'bang';
import config from '../../../config'

export async function createSale (ctx) {
  const Sale = ctx.app.models.sale;
  const Article = ctx.app.models.article;
  const sale = ctx.request.body;

  const saleToday = {
    clientId: sale.userId,
    total: sale.total,
    date: new Date()
  };

  for (const article of sale.articles) {
    const { data: currentArticle, err: errCuArt } = await to(Article.findById(article.id));
    if (errCuArt) throw Bang.wrap(errCuArt);

    const newQuantity = Number(currentArticle.existence) - Number(article.quantity);
    const { data: updated, err: errArt } = await to(Article.updateById(article.id, { existence: newQuantity }));
    if (errArt) throw Bang.wrap(errArt);
  }

  const { data: newSale, err: errSale } = await to(Sale.create(saleToday));
  if (errSale) throw Bang.wrap(errSale);

  ctx.body = newSale;
}


export async function getSales (ctx) {
  const Sale = ctx.app.models.sale;

  const filter = {
    include: 'client'
  };

  const { data: sales, err } = await to(Sale.find(filter));
  if (err) throw Bang.wrap(err);

  ctx.body = sales;
}
