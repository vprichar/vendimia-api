import * as course from './controller'

export const baseUrl = '/sales'

export default [
  {
    method: 'POST',
    route: '/',
    handlers: [
      course.createSale
    ],
    permissions: '*'
  },
  {
    method: 'GET',
    route: '/',
    handlers: [
      course.getSales
    ],
    permissions: '*'
  }
]
